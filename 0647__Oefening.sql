USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleasesLoop`(
	IN extraReleases INT
)
BEGIN
	DECLARE counter INT DEFAULT 0;
    MockAlbumReleasesLoop : LOOP
        SET counter = counter +1;
        IF counter > extraReleases
		THEN LEAVE MockAlbumReleasesLoop;
        ELSE CALL MockAlbumReleaseWithSuccess(@success);
        END IF;
	END LOOP;
END$$

DELIMITER ;

