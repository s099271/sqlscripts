USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(
		IN someDate DATE, OUT numbersCleaned INT
)
BEGIN
	DECLARE BeforeExecution, AfterExecution INT;
	SELECT COUNT(*) INTO BeforeExecution FROM Lidmaatschappen;
    SET SQL_SAFE_UPDATES = 0;
    DELETE FROM Lidmaatschappen
    WHERE EindDatum IS NOT NULL AND EindDatum < someDate;
    SET SQL_SAFE_UPDATES = 1;
    SELECT COUNT(*) INTO AfterExecution FROM Lidmaatschappen;
    SET numbersCleaned = BeforeExecution - AfterExecution;
END$$

DELIMITER ;

