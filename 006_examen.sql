CREATE USER IF NOT EXISTS examen IDENTIFIED BY 'corona';
GRANT EXECUTE ON PROCEDURE aptunes_examen.GetSongDuration TO examen;
GRANT SELECT ON TABLE aptunes_examen.Liedjes TO examen;