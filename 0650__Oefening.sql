USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumReleases` ()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId1 INT DEFAULT 0;
    DECLARE randomBandId1 INT DEFAULT 0;
    DECLARE randomAlbumId2 INT DEFAULT 0;
    DECLARE randomBandId2 INT DEFAULT 0;
    DECLARE randomAlbumId3 INT DEFAULT 0;
    DECLARE randomBandId3 INT DEFAULT 0;
    DECLARE randomValue TINYINT DEFAULT 0;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		ROLLBACK;
        SELECT 'Nieuwe releases konden niet worden toegevoegd';
    END;
    SELECT COUNT(*) INTO numberOfAlbums FROM Albums;
    SELECT COUNT(*) INTO numberOfBands FROM Bands;
    SET randomAlbumId1 = floor(rand()*numberOfAlbums) + 1;
    SET randomAlbumId2 = floor(rand()*numberOfAlbums) + 1;
    SET randomAlbumId3 = floor(rand()*numberOfAlbums) + 1;
    SET randomBandId1 = floor(rand()*numberOfBands) + 1;
    SET randomBandId2 = floor(rand()*numberOfBands) + 1;
    SET randomBandId3 = floor(rand()*numberOfBands) + 1;
    START TRANSACTION;
    INSERT INTO Albumreleases(Bands_Id,Albums_Id) 
    VALUES 
    (randomBandId1,randomAlbumId1),
    (randomBandId2,randomAlbumId2);
    SET randomValue = floor(rand() * 3) + 1;
    IF randomValue = 1 THEN
		SIGNAL SQLSTATE '45000';
	END IF;
	INSERT INTO Albumreleases(Bands_Id,Albums_Id)
    VALUES (randomBandId3,randomAlbumId3);
	COMMIT;
END$$

DELIMITER ;

