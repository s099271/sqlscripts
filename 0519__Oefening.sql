USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE 
	Liedjes
ADD COLUMN Genre VARCHAR(20);
UPDATE Liedjes SET Genre = 'Hard Rock' WHERE Artiest = 'Led Zeppelin' OR Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 1;