USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE 
	Huisdieren 
ADD COLUMN Geluid VARCHAR(20);
UPDATE Huisdieren SET Geluid = 'WAF!' WHERE Soort = 'Hond';
UPDATE Huisdieren SET Geluid = 'miauwww...' WHERE Soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;