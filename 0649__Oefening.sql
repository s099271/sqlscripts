USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
	DECLARE randomValue TINYINT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
    END;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
    BEGIN
		SELECT 'State 45002 opgevangen. Geen probleem';
    END;
    SET randomValue = floor(rand()*3) + 1;
    IF randomValue = 1 THEN
		SIGNAL SQLSTATE '45001';
	ELSEIF randomValue = 2 THEN
		SIGNAL SQLSTATE '45002';
	ELSE
		SIGNAL SQLSTATE '45003';
	END IF;
     
END$$

DELIMITER ;