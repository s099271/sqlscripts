USE ModernWays;
SELECT Leden.Voornaam, Boeken.Titel
FROM Uitleningen
	INNER JOIN Boeken ON Uitleningen.Boeken_Id = Boeken.Id
    INNER JOIN Leden ON Uitleningen.Leden_Id = Leden.Id