USE `aptunes`;
DROP procedure IF EXISTS `aptunes`.`GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAlbumDuration`(
IN album INT, OUT totalDuration SMALLINT UNSIGNED
)
BEGIN
	DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
	DECLARE ok BOOL DEFAULT FALSE;
    DECLARE songDurationCursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = album;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = TRUE;
    SET totalDuration = 0;
    OPEN songDurationCursor;
    fetchloop: LOOP
		FETCH songDurationCursor INTO songDuration;
        IF ok = TRUE THEN 
			LEAVE fetchloop; 
		END IF;
		SET totalDuration = totalDuration + songDuration;
    END LOOP;
    CLOSE songDurationCursor;
END$$

DELIMITER ;
;
