USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(
	IN titelAlbum VARCHAR(50)
)
BEGIN
    SELECT Titel
    FROM liedjes
    WHERE Titel LIKE CONCAT('%', titelAlbum, '%');
END$$

DELIMITER ;