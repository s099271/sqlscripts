use ModernWays;
insert into Personen (
   Voornaam, Familienaam, AanspreekTitel
)
values (
   'Hilary', 'Mantel', 'Mevrouw'
);
insert into Boeken (
   Titel, Stad, Uitgeverij, Verschijningsdatum,
   Herdruk, Commentaar, Categorie, Personen_Id
)
values (
   'Wolf Hall', '', 'Fourth Estate; First Picador Edition First Printing edition',
   '2010', '', 'Goed boek', 'Thriller', 11
);