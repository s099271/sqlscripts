USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleaseWithSuccess`(
	OUT success BOOLEAN
)
BEGIN
	DECLARE numberOfAlbums INT(0);
    DECLARE numberOfBands INT(0);
    DECLARE randomAlbumId INT(0); 
    DECLARE randomBandId INT(0);
	SELECT COUNT(*)
    INTO numberOfAlbums	
    FROM Albums;
    SELECT COUNT(*)
    INTO numberOfBands	
    FROM Bands;
    
    SELECT FLOOR(RAND() * numberOfAlbums) + 1
    INTO randomAlbumId;
    SELECT FLOOR(RAND() * numberOfBands) + 1
    INTO randomBandId;
    IF (SELECT COUNT(*) FROM Albumreleases WHERE Bands_Id = randomBandId && Albums_Id = randomAlbumId) = 0 
    THEN 
		INSERT INTO Albumreleases(Bands_Id, Albums_Id) VALUES (randomBandId,randomAlbumId);
		SET success = 1;
	ELSE
		SET success = 0;
    END IF;
END$$

DELIMITER ;

