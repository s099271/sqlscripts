USE ModernWays;
CREATE VIEW AuteursBoeken AS 
SELECT CONCAT(Voornaam, ' ',Familienaam) AS Auteur, Titel
FROM boeken INNER JOIN publicaties ON boeken.Id = publicaties.Boeken_Id
INNER JOIN personen ON publicaties.Personen_Id = personen.Id;