CREATE VIEW GemiddeldeRatings AS 
SELECT Boeken_Id, AVG(Rating) AS Ratings
FROM Reviews GROUP BY Boeken_Id