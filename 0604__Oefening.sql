USE ModernWays;
CREATE TABLE IF NOT EXISTS Uitleningen(
	Leden_Id INT(11) NOT NULL,
    Boeken_Id INT(11) NOT NULL,
    StartDatum DATE NOT NULL,
    EindDatum DATE,
    KEY fk_Uitleningen_Leden (Leden_Id),
    KEY fk_Uitleningen_Boeken (Boeken_ID),
	CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) REFERENCES Leden (Id),
    CONSTRAINT FK_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) REFERENCES boeken (Id)
)