USE ModernWays;
SELECT Games.Titel, Platformen.Naam
FROM Releases
	RIGHT JOIN Games ON Releases.Games_Id = Games.Id
    LEFT JOIN Platformen ON Releases.Platformen_Id = Platformen.Id