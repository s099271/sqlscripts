USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(
	OUT Aantal TINYINT
)
BEGIN
	SELECT COUNT(*)
    INTO Aantal
    FROM Genres;
END$$

DELIMITER ;