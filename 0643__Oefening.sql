USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(
	IN Titel VARCHAR(100), IN Bands_Id INT
)
BEGIN
	INSERT INTO Albums (Titel) VALUES (Titel);
    INSERT INTO AlbumReleases (Bands_Id, Albums_Id) VALUES (Bands_Id, LAST_INSERT_ID());
END$$

DELIMITER ;