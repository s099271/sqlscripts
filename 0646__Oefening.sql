USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleases`(
	IN extraReleases INT
)
BEGIN
	DECLARE counter INT DEFAULT 0;
    REPEAT
		CALL MockAlbumReleaseWithSuccess(@success);
        SET counter = counter +1;
	UNTIL counter = extraReleases
    END REPEAT;
END$$

DELIMITER ;

