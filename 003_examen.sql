USE `aptunes_examen`;
DROP procedure IF EXISTS `PopulateLiedjesGenres`;

DELIMITER $$
USE `aptunes_examen`$$
CREATE PROCEDURE `PopulateLiedjesGenres` ()
BEGIN
	DECLARE numberOfLiedjes INT(0);
    DECLARE numberOfGenres INT(0);
    DECLARE randomLiedjeId INT(0); 
    DECLARE randomGenreId INT(0);
	SELECT COUNT(*)
    INTO numberOfLiedjes	
    FROM Liedjes;
    SELECT COUNT(*)
    INTO numberOfGenres	
    FROM Genres;
    
    SELECT FLOOR(RAND() * numberOfLiedjes) + 1
    INTO randomLiedjeId;
    SELECT FLOOR(RAND() * numberOfGenres) + 1
    INTO randomGenreId;
    
    IF (SELECT COUNT(*) FROM LiedjesGenres WHERE Liedjes_Id = randomLiedjeId && Genres_Id = randomGenreId) = 0 
    THEN INSERT INTO LiedjesGenres(Liedjes_Id, Genres_Id) VALUES (randomLiedjeId,randomGenreId);
    END IF;
END$$

DELIMITER ;