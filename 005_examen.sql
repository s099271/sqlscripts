USE `aptunes_examen`;
DROP procedure IF EXISTS `GetSongDuration`;

DELIMITER $$
USE `aptunes_examen`$$
CREATE PROCEDURE `GetSongDuration`(
IN band INT
)
SQL SECURITY INVOKER
BEGIN
	DECLARE totalDuration SMALLINT UNSIGNED;
	DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
	DECLARE ok BOOL DEFAULT FALSE;
    DECLARE songDurationCursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Bands_Id = band;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = TRUE;
    SET totalDuration = 0;
    OPEN songDurationCursor;
    fetchloop: LOOP
		FETCH songDurationCursor INTO songDuration;
        IF ok = TRUE THEN 
			LEAVE fetchloop; 
		END IF;
		SET totalDuration = totalDuration + songDuration;
    END LOOP;
    CLOSE songDurationCursor;
    IF totalDuration > 60 THEN 
		SELECT 'Lange duurtijd' AS Duurtijd;
	ELSE 
		SELECT 'Normale duurtijd' AS Duurtijd;
	END IF;
END$$

DELIMITER ;
;
