ALTER VIEW AuteursBoeken AS
SELECT CONCAT(Voornaam,' ', Familienaam ) AS Auteur, Titel, boeken.Id AS Boeken_Id
FROM boeken INNER JOIN publicaties ON boeken.Id = publicaties.Boeken_Id
INNER JOIN personen ON publicaties.Personen_Id = personen.Id;
