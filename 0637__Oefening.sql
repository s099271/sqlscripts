use ModernWays;

-- Nieuwe tabel creeeren voor Personen
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- Gegevens invullen in de nieuwe tabel a.d.h.v. tabel Boeken
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

-- toevoegen van overige kolommen aan table Persoon
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
-- Toevoegen van Personen_Id tabel aan boeken (wordt later gewijzigd naar not null)
alter table Boeken add Personen_Id int null;

-- Toevoegen van Personen_Id in boeken a.d.h.v. de toegewezen Id in tabel Personen
set sql_safe_updates=0;
update Boeken cross join Personen
	set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
set sql_safe_updates=1;

-- Tabel Personen_Id wijzigen naar not null
alter table Boeken change Personen_Id Personen_Id int not null;

-- Dubbele kolommen verwijderen van tabel Boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;

-- Toewijzen van de Foreign key aan de kolom Personen_Id in tabel Boeken
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);


