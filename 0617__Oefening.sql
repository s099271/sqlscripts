CREATE VIEW AuteursBoekenRating as
SELECT Auteur, Titel, Ratings 
FROM AuteursBoeken INNER JOIN GemiddeldeRatings ON AuteursBoeken.Boeken_ID = GemiddeldeRatings.Boeken_Id